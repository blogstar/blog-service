package ru.stankin.mikaev.blogstar.profileservice.controller;

import lombok.RequiredArgsConstructor;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.stankin.mikaev.blogstar.profileservice.dto.ProfileInfoDto;
import ru.stankin.mikaev.blogstar.profileservice.service.ProfileService;
import ru.stankin.mikaev.blogstar.profileservice.service.SecurityService;

/**
 * BlogController.
 *
 * @author Nikita_Mikaev
 */
@RestController
@RequestMapping("/api/v1/profile")
@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
public class ProfileController {

    private final ProfileService profileService;

    @GetMapping("/info")
    public ProfileInfoDto getProfileInfo() {
        return profileService.getProfileInfo();
    }
}
