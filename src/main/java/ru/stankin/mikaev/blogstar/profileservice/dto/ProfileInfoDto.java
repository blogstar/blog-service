package ru.stankin.mikaev.blogstar.profileservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ProfileInfoDto.
 *
 * @author Nikita_Mikaev
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProfileInfoDto {

    private String id;

    private String firstName;

    private String lastName;

    private String username;
}
