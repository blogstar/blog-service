package ru.stankin.mikaev.blogstar.profileservice.service;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Service;
import ru.stankin.mikaev.blogstar.profileservice.dto.ProfileInfoDto;

/**
 * ProfileService.
 *
 * @author Nikita_Mikaev
 */
@Service
public class ProfileService {

    public ProfileInfoDto getProfileInfo() {
        KeycloakPrincipal<KeycloakSecurityContext> principal = SecurityService.getKeycloakPrincipal();
        AccessToken accessToken = principal.getKeycloakSecurityContext().getToken();
        // first name
        String firstName = accessToken.getGivenName();
        // last name
        String lastName = accessToken.getFamilyName();
        return ProfileInfoDto.builder().id(SecurityService.getUserId())
                .firstName(firstName)
                .lastName(lastName)
                .username(principal.getName())
                .build();
    }
}
