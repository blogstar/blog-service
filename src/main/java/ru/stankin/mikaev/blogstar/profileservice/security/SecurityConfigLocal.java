package ru.stankin.mikaev.blogstar.profileservice.security;

import static ru.stankin.mikaev.blogstar.profileservice.constants.ServiceConstants.TEST_PROFILE;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * SecurityConfigLocal.
 *
 * @author Nikita_Mikaev
 */
@Configuration
@EnableWebSecurity
@Profile({TEST_PROFILE})
@Slf4j
public class SecurityConfigLocal extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("Run TEST Security Configuration");
    }
}