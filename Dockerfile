FROM adoptopenjdk/openjdk11:alpine-jre

ADD target/profileservice-0.0.1-SNAPSHOT.jar /app.jar

ENTRYPOINT exec java $JAVA_OPTS -jar /app.jar